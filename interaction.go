package hpmanager

import (
	"context"
	"net/http"
	"strconv"

	"github.com/slack-go/slack"
	hp "gitlab.com/johnrichter/house-points"
	hpevent "gitlab.com/johnrichter/house-points/event"
	"gitlab.com/johnrichter/logging-go"
	"gitlab.com/johnrichter/slack-app"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

var (
	respUnableToProcessPointsSubmission = slackapp.SlackCallbackResponse{
		StatusCode: http.StatusOK,
		Data: slack.Msg{
			ResponseType: "ephemeral",
			Attachments: []slack.Attachment{
				{
					AuthorName: "House Points",
				},
				{
					Text:     "We're currently unable to process your House Points submission. Pleease try again later.",
					Fallback: "We're currently unable to process your House Points submission. Pleease try again later.",
				},
			},
		},
	}
)

func GenAwardPointsEventID(cb *slack.InteractionCallback, ds *hpevent.HousePointsDialogSubmission) string {
	var action string
	switch ds.Action {
	case hpevent.EventActionAdd:
		action = hpevent.EventActionAdd
	case hpevent.EventActionRemove:
		action = hpevent.EventActionRemove
	}
	return hpevent.GenID(
		"%s.%s.%s.%s.%s.%s.%d.%s.%s.%s",
		hpevent.EventTypeAwardPoints, hpevent.EventOriginSlack, cb.APIAppID, cb.Team.ID, cb.Channel.ID, action,
		ds.Amount, ds.Recipient, cb.User.ID, ds.Reason,
	)
}

func (s *HousePointsService) handleSupportedInteraction(
	ctx context.Context,
	ic *slack.InteractionCallback,
) (*slackapp.SlackCallbackResponse, error) {
	// HousePoints does not implement DialogSuggestion, InteractionMessage, or BlockActions
	switch ic.Type {
	case slack.InteractionTypeDialogSubmission:
		return s.handlePointsDialogSubmission(ctx, ic)
	case slackapp.SlackInteractionTypeGlobalShortcut:
		return s.openGlobalPointSubmisionDialog(ctx, ic.TriggerID, ic.Message.User, ic.Message.Text)
	case slack.InteractionTypeMessageAction:
		return s.openMessagePointSubmisionDialog(ctx, ic.TriggerID, ic.Message.User, ic.Message.Text)
	case slack.InteractionTypeDialogCancellation: // No-op for House Points
	default:
		hplog.Debug().
			Str(logging.SlackInteractionID, ic.CallbackID).
			Str(logging.SlackInteractionType, string(ic.Type)).
			Msg("Unknown slack interaction type")
	}
	return &slackapp.SuccessNoContent, nil
}

func (s *HousePointsService) handlePointsDialogSubmission(
	ctx context.Context,
	cb *slack.InteractionCallback,
) (*slackapp.SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.interaction.house_points.dialog_submission")
	defer span.Finish()

	ds, err := hpevent.NewHousePointsDialogSubmission(cb.Submission)
	if err != nil {
		return nil, &slackapp.ErrMalformedDialogSubmission
	}
	eid := GenAwardPointsEventID(cb, ds)
	hpe, err := hpevent.WrapHousePointsSlackInteraction(eid, hpevent.EventTypeAwardPoints, cb)
	if err != nil {
		return nil, &slackapp.ErrInternalServerError
	}
	if err := s.eventBus.AddEvent(ctx, hpe); err != nil {
		hplog.Error().Err(err).Msg("Unable to push event to bus")
		return &respUnableToProcessPointsSubmission, nil
	}
	msg := slack.Msg{
		ResponseType: "ephemeral",
		Attachments: []slack.Attachment{
			{
				AuthorName: "House Points",
			},
			{
				Text:     "Thanks for your House Points Submission. We're currently processing the award and will let you know when it's been accepted.",
				Fallback: "Thanks for your House Points Submission. We're currently processing the award and will let you know when it's been accepted.",
			},
		},
	}
	// Responding directly to Slack with a message seems to break things so we'll post it instead
	PostSlackMsgToURL(ctx, cb.ResponseURL, &msg)
	return &slackapp.SuccessNoContent, nil
}

func (s *HousePointsService) openGlobalPointSubmisionDialog(
	ctx context.Context,
	triggerID,
	user,
	reason string,
) (*slackapp.SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.interaction.house_points.dialog_open")
	defer span.Finish()

	d := s.createPointSubmissionDialog(hp.HousePointsInteractionIDGlobal, triggerID, user, reason)
	if err := s.slack.OpenDialogContext(ctx, triggerID, *d); err != nil {
		hplog.Error().Err(err).Msg("Failed to open global points submission dialog")
		return nil, &slackapp.ErrSlackAPIDialog
	}
	return &slackapp.SuccessNoContent, nil
}

func (s *HousePointsService) openMessagePointSubmisionDialog(
	ctx context.Context,
	triggerID,
	user,
	reason string,
) (*slackapp.SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.interaction.house_points.dialog_open")
	defer span.Finish()

	d := s.createPointSubmissionDialog(hp.HousePointsInteractionIDMessage, triggerID, user, reason)
	if err := s.slack.OpenDialogContext(ctx, triggerID, *d); err != nil {
		hplog.Error().Err(err).Msg("Failed to open message points submission dialog")
		return nil, &slackapp.ErrSlackAPIDialog
	}
	return &slackapp.SuccessNoContent, nil
}

func (s *HousePointsService) createPointSubmissionDialog(callbackID, triggerID, user, reason string) *slack.Dialog {
	// TODO: Add state to the dialog when we upgrade nlopes/slack
	// state := fmt.Sprintf("%d:%s:%s", 1, "", command.Command)
	// hashedState := sp.secrets.HashWithHMAC([]byte(state))

	amtOpts := []map[string]string{}
	for amount := 1; amount <= slackapp.SlackMaxDialogSelectOptions; amount++ {
		amountA := strconv.Itoa(amount)
		amtOpts = append(amtOpts, map[string]string{"label": amountA, "value": amountA})
	}
	d := slack.Dialog{
		TriggerID:  triggerID,
		CallbackID: callbackID,
		// State:          hex.EncodeToString(h.Sum(nil)),
		Title:          "Award House Points!",
		SubmitLabel:    "Submit",
		NotifyOnCancel: true,
		Elements: []slack.DialogElement{
			map[string]interface{}{
				"type":        "select",
				"label":       "Action",
				"name":        "action",
				"data_source": "static",
				"options": []map[string]string{
					{"label": "Give", "value": "add"},
					{"label": "Take Away", "value": "remove"},
				},
			},
			map[string]interface{}{
				"type":    "select",
				"label":   "Amount",
				"name":    "amount",
				"options": amtOpts,
			},
			map[string]interface{}{
				"type":        "select",
				"label":       "Recipient",
				"name":        "recipient",
				"data_source": slack.DialogDataSourceUsers,
				"value":       user,
			},
			map[string]interface{}{
				"type":        "textarea",
				"label":       "Reason",
				"name":        "reason",
				"placeholder": "They #sentit like a pro",
				"max_length":  3000,
				"min_length":  1,
				"optional":    false,
				"hint":        "Emojis can be used",
				"value":       reason,
			},
		},
	}
	return &d
}
