# House Points Manager

Manages a friendly House Points competition between family, friends, coworkers, and more.

## Purpose

This library is meant to be plugged into the [Slack App template library](https://gitlab.com/johnrichter/slack-app). It
handles all Slack interactions, events, and callbacks specific to the House Points game. Any longer running work that
needs to be done is defined by a
[House Points Event](https://gitlab.com/johnrichter/house-points/-/tree/master/event), sent over the
[event bus](https://gitlab.com/johnrichter/house-points/-/tree/master/event/bus), and performed by the
[House Points Worker](https://gitlab.com/johnrichter/house-points-worker).

# Development

Given the polyrepo setup of the House Points components, you may need to specify local locations for one or more
dependencies. Use the `go.mod.local` file as a guide to build and test this repo locally

In addition, see the [contribution guidelines](CONTRIBUTING.md).
