### Go Modules

This repository currently takes an [idiosyncratic approach](https://github.com/DataDog/dd-trace-go/issues/810) to using Go modules which means that you should not commit modified versions of the `go.mod` or `go.sum` files.

The following git command can be used to permanently ignore modifications to these files:

```
git update-index --assume-unchanged go.*
```

If you need to undo this for any reason, you can run:

```
git update-index --no-assume-unchanged go.*
```
