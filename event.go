package hpmanager

import (
	"context"

	"github.com/slack-go/slack/slackevents"
	hpevent "gitlab.com/johnrichter/house-points/event"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

func (s *HousePointsService) handleSlackLinkSharedEvent(
	ctx context.Context,
	cbe *slackevents.EventsAPICallbackEvent,
	e *slackevents.LinkSharedEvent,
) error {
	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.event.house_points.link_shared")
	defer span.Finish()

	hpe, err := hpevent.WrapHousePointsSlackEventsAPIEvent(hpevent.EventTypeLinkShared, cbe)
	if err != nil {
		return err
	}
	if err := s.eventBus.AddEvent(ctx, hpe); err != nil {
		hplog.Error().Err(err).Msg("Unable to push event to bus")
		return err
	}
	return nil
}

func (s *HousePointsService) handleSlackReactionAddedEvent(
	ctx context.Context,
	cbe *slackevents.EventsAPICallbackEvent,
	re *slackevents.ReactionAddedEvent,
) error {
	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.event.house_points.reaction_added")
	defer span.Finish()

	hpe, err := hpevent.WrapHousePointsSlackEventsAPIEvent(hpevent.EventTypeReaction, cbe)
	if err != nil {
		return err
	}
	if err := s.eventBus.AddEvent(ctx, hpe); err != nil {
		hplog.Error().Err(err).Msg("Unable to push event to bus")
		return err
	}
	return nil
}

func (s *HousePointsService) handleSlackReactionRemovedEvent(
	ctx context.Context,
	cbe *slackevents.EventsAPICallbackEvent,
	re *slackevents.ReactionRemovedEvent,
) error {
	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.event.house_points.reaction_removed")
	defer span.Finish()

	hpe, err := hpevent.WrapHousePointsSlackEventsAPIEvent(hpevent.EventTypeReaction, cbe)
	if err != nil {
		return err
	}
	if err := s.eventBus.AddEvent(ctx, hpe); err != nil {
		hplog.Error().Err(err).Msg("Unable to push event to bus")
		return err
	}
	return nil
}
