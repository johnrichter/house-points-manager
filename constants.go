package hpmanager

import (
	"github.com/slack-go/slack/slackevents"
	hp "gitlab.com/johnrichter/house-points"
)

var (
	housePointsEnabledSlashCommands = []string{hp.HousePointsSlashCommandPoints}
	housePointsEnabledInteractions  = []string{
		hp.HousePointsInteractionIDGlobal,
		hp.HousePointsInteractionIDMessage,
	}
	housePointsEnabledEventTypes = []string{
		slackevents.LinkShared,
		slackevents.ReactionAdded,
		slackevents.ReactionRemoved,
	}
)
